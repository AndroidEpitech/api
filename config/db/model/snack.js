const mongoose = require('mongoose');
const Schema = mongoose.Schema
const shortid = require('shortid');
const bcrypt = require('bcrypt');

const snackSchema = new Schema({
  _id: {type: String, 'default': shortid.generate},
  created: {type: Date, default: Date.now},
  updated: {type: Date, default: Date.now},
  name: String,
  image: String,
  price: Number,
  desc: String,
  promo: Boolean,
  hard: Boolean
})

const Snack = mongoose.model('Snack', snackSchema)

module.exports = Snack
