const mongoose = require('mongoose');
const Schema = mongoose.Schema
const shortid = require('shortid');
const bcrypt = require('bcrypt');
const Snack = require('./snack.js')

const userSchema = new Schema({
  _id: {type: String, 'default': shortid.generate},
  created: {type: Date, default: Date.now},
  updated: {type: Date, default: Date.now},
  androidId: String,
  firstName: String,
  lastName: String,
  mail: {type: String, index: true, required: true},
  password: {type: String, required: true},
  cinema: String,
  room: String,
  seat: String,
  creditCard: {
    sn: String,
    exp: String,
    secure: String
  },
  creditAmount: {type: Number, 'default': 10},
  // gps:
  orders: [{
      _id: {type: String, 'default': shortid.generate},
      created: {type: Date, default: Date.now},
      items: [{
        _id: {type: String, 'default': shortid.generate},
        snack: String,
        quantity: Number
      }],
      totalAmount: Number
  }]
})

userSchema.pre('save', function(next){
  const user = this

  const saltRound = Math.random() * (10 - 7) + 7; //random saltRound btw 7 - 14

  if (this.isModified('password') || this.isNew) {
      bcrypt.genSalt(saltRound, function(err, salt){
          if (err) return next(err)
          bcrypt.hash(user.password, salt, function(err, hash){
              if (err) return next(err)
              user.password = hash
              next()
          })
      })
  } else {
      return next()
  }
})


// userSchema.pre('update', function(next){
//   const user = this
//
//   const saltRound = Math.random() * (10 - 7) + 7; //random saltRound btw 7 - 14
//
//     if (this.isModified('password') || this.isNew) {
//         bcrypt.genSalt(saltRound, function(err, salt){
//             if (err) return next(err)
//             bcrypt.hash(user.password, salt, function(err, hash){
//                 if (err) return next(err)
//                 user.password = hash
//                 next()
//             })
//         })
//     } else {
//         return next()
//     }
// })

userSchema.methods.comparePassword = function(clearPass, callback) {
    bcrypt.compare(clearPass, this.password, function(err, isMatch) {
        if (err) {
            return callback(err)
        }
        callback(null, isMatch)
    });
};

const User = mongoose.model('User', userSchema)

module.exports = User
