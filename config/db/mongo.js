let mongo = {};
const usernameDB = process.env.MONGO_USER;
const passDB = process.env.MONGO_PASS;

mongo.mongoURI = {
  development: 'mongodb://' + usernameDB + ':' + passDB + '@ds060749.mlab.com:60749/cinesnack',
  // test: 'mongodb://localhost/bikeTrackTesting'
};

mongo.jwt = {
  secret: "42Epitech !!!"
}

module.exports = mongo;
