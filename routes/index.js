const express = require('express')
const jwt = require('jsonwebtoken')
const router = express.Router()
const config = require('../config/db/mongo')
const User = require('../config/db/model/user')
const Snack = require('../config/db/model/snack')


const ApiKey = 'AIzaSyC8K57jwE5xTSPoSrR7x2X6L6aGnTd4V_g';
const registrationToken = ['8fd566bdf914130e'];

const serviceAccount = require("../cinesnack-api-firebase.json");

const admin = require("firebase-admin");

admin.initializeApp({
  credential: admin.credential.cert(serviceAccount),
  databaseURL: "https://cinesnack-api.firebaseio.com"
})

// Test
router.get('/', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.json({foo: 'bar'})
})

router.post('/', (req, res) => {
    res.setHeader('Content-Type', 'application/json')
    res.json({foo2: 'bar2'})
})

router.post('/register', register)
router.post('/login', login)
// router.post('/newpwd', newPwd)
router.post('/notif', sendNotif)

router.post('/snack', addSnack)


router.use((req, res, next) => {
  let token = req.headers['x-access-token'];

  if (token) {
    jwt.verify(token, config.jwt.secret, (err, decoded) => {
      if (err) {
          return res.json({success: false, err});
      } else {
          // if everything is good, save to request for use in other routes
          req.decoded = decoded;
          next();
      }
    })
    } else {
        // if there is no token
        // return an error
        return res.status(403).send({success: false, message: 'No token provided.'});
    }
})

router.get('/profile/:id', getProfile)
router.patch('/profile/:id', updateProfile)
router.patch('/profile/:id/cb', updateCB)
router.patch('/profile/:id/addCredit', addCredit)
router.get('/snack/id/:id', getSnack)
router.get('/snack/promo', getPromo)
router.get('/snack/hard', getHard)
router.get('/snack/soft', getSoft)
router.post('/order', makeOrder)
router.get('/profile/:id/history', getOrderHistory)

/*
//
//  MAGIC FUNCTION
//
*/

function send404err(err, res) {
  res.status(400)
  res.json({success: false, err})
  res.end()
}

function sen401UserNotFound(res) {
  res.status(401)
  res.json({success: false, message: 'User not found.'})
  res.end()
}

function send400InvalidParams(res) {
  res.status(400)
  res.json({
    success: false,
    message: 'Invalid parameters'
  })
  res.end()
}

// router.post('/notif', sendNotif)
function sendNotif(req, res) {


  const payload = {
    data: {
      score: "850",
      time: "2:45"
    }
  };

  admin.messaging().sendToDevice(registrationToken, payload)
    .then(function(response) {
      // See the MessagingDevicesResponse reference documentation for
      // the contents of response.
      console.log("Successfully sent message:", response);
      res.json({success:true, response})
      res.end()

    })
    .catch(function(error) {
      console.log("Error sending message:", error);
      res.json({success:false})
      res.end()
    });
}

/*
//
//  REGISTER/LOGIN
//
*/

// router.post('/register', register)
function register(req, res) {
  if (req.body && req.body.mail && req.body.password && req.body.androidId) {
    const mail = req.body.mail
    const password = req.body.password
    const androidId = req.body.androidId

    if (!mail || !password || !androidId) {
        res.status(404)
        res.json({success: false, message: "Mail || Passowrd || androidId is blank", mail: mail, password: password, androidId:androidId})
        res.end()
    }

    User.findOne({ mail }, (err, user) => {
      if (err) {
        send404err(err, res)
      } else if (user && (user.mail == mail)) {
        res.status(400);
    		res.json({success: false, message: 'User already in DB'})
        res.end()
      } else if (!user) {
        const newUser = new User({mail: mail, password: password, androidId: androidId})
        newUser.save(err => {
          if (err) {
            send404err(err, res)
          }
          res.json({success: true, message: 'Try to log now !'})
          res.end()
        })
      }
    })
  } else {
    send400InvalidParams(res)
  }
}

// router.post('/login', login)
function login(req, res) {
  if (req.body && req.body.mail && req.body.password && req.body.androidId) {
    const mail = req.body.mail
    const password = req.body.password
    const androidId = req.body.androidId

    if (!mail || !password || !androidId) {
        res.status(404)
        res.json({success: false, message: "Mail || Passowrd || androidId is blank"})
        res.end()
    }

    User.findOne({mail}, (err, user) => {
      if (err) {
          send404err(err, res)
      } else if (!user) {
          sen401UserNotFound(res)
      } else if (user) {
        const comparePassword = user.comparePassword(password, (err, isMatch) => {
          if (err) {
            send404err(err, res)
          } else if (!isMatch) {
            res.status(401)
            res.json({success: false, message: "Authentication failed. Wrong password."})
            res.end()
          } else {
            const token = jwt.sign(user, config.jwt.secret)
            res.json({success: true, token: token, userId: user.id, message: "Use the token my lord"})
            res.end()
          }
        })
      }
    })
  } else {
    send400InvalidParams(res)
  }
}

// // router.post('/newpwd', newPwd)
// function newPwd(req, res) {
//   if (req.body && req.body.mail) {
//     const mail = req.body.mail
//
//     if (!mail) {
//         res.status(404)
//         res.json({success: false, message: "Mail is blank"})
//         res.end()
//     }
//
//     User.findOne({mail}, (err, user) => {
//       if (err) {
//           res.status(400)
//           res.json({success: false, err})
//           res.end()
//       } else if (!user) {
//           res.status(401)
//           res.json({success: false, message: 'Authentication failed. User not found.'})
//           res.end()
//       } else if (user) {
//         console.log(user);
//           user.update({password: 'reset'}, err => {
//             if (err) {
//               res.status(400)
//               res.json({success: false, err})
//               res.end()
//             } else {
//               res.json({success: true, message: `Mail: ${user.mail} / Password: ${user.password})`})
//               res.end()
//             }
//           })
//       }
//     })
//   }
// }


/*
//
//  PROFILE ROUTE
//
*/


// router.get('/profile/:id', getProfile)
function getProfile(req, res) {
  if (req.params && req.params.id) {
    const userId = req.params.id

    if (!userId) {
      res.status(404)
      res.json({success: false, message: "userId is blank"})
      res.end()
    }

    User.findById(userId, '-password', (err, user) => {
      if (err) {
        send404err(err, res)
      } else if (!user) {
        sen401UserNotFound(res)
      } else if (user) {
        res.json({success: true, message: user})
      }
    })
  } else {
    send400InvalidParams(res)
  }
}

//router.patch('/profile/:id', updateProfile)
function updateProfile(req, res) {
  if (req.params && req.params.id && req.body && req.body.userInfo) {

    const userInfo = req.body.userInfo
    const userId = req.params.id

    if (!userInfo || !userId) {
      res.status(404)
      res.json({success: false, message: "userInfo is blank"})
      res.end()
    }

    User.findByIdAndUpdate(userId, userInfo, (err, user) => {
      if (err) {
        send404err(err, res)
      } else if (!user) {
        sen401UserNotFound(res)
      } else if (user) {
         res.json({success: true, message: `User ${user._id} updated`})
         res.end()
      }
    })

  } else {
    send400InvalidParams(res)
  }
}

// router.patch('/profile/:id/cb', updateCB)
function updateCB(req, res) {
  if (req.params && req.params.id && req.body && req.body.creditCard) {

    const creditCard = req.body.creditCard
    const userId = req.params.id

    if (! creditCard || !userId) {
      res.status(404)
      res.json({success: false, message: "userInfo is blank"})
      res.end()
    }

    User.findByIdAndUpdate(userId, {creditCard:creditCard}, (err, user) => {
      if (err) {
        send404err(err, res)
      } else if (!user) {
        sen401UserNotFound(res)
      } else if (user) {
        res.json({success: true, message: `User ${user._id} updated`})
        res.end()
      }
    })

  } else {
    send400InvalidParams(res)
  }
}

//router.patch('/profile/:id/addCredit', addCredit)
function addCredit(req, res) {
  if (req.params && req.params.id && req.body && req.body.creditAmount) {

    const creditAmount = req.body.creditAmount
    const userId = req.params.id

    if (!creditAmount || !userId) {
      res.status(404)
      res.json({success: false, message: "creditAmount || userId is blank"})
      res.end()
    }

    User.findById(userId, (err, user) => {
      if (err) {
        send404err(err, res)
      } else if (!user) {
        sen401UserNotFound(res)
      } else if (user) {
        const newAmount = user.creditAmount + creditAmount
        user.update({creditAmount:newAmount}, err => {
          if (err) {
            res.status(400);
        		res.json({success: false, err})
            res.end()
          }
          res.json({success: true, message: 'Credit Added !'})
          res.end()
        })
      }
    })

  } else {
    send400InvalidParams(res)
  }
}

/*
//
//  SNACK ROUTE
//
*/


// router.get('/snack/id/:id', getSnack)
function getSnack(req, res) {
  if (req.params && req.params.id) {

    const snackId = req.params.id

    Snack.findById(snackId, (err, snack) => {
      if (err) {
        send404err(err, res)
      } else if (!snack) {
        sen401UserNotFound(res)
      } else if (snack) {
        res.json({success: true, snack})
        res.end()
      }
    })
  } else {
    send400InvalidParams(res)
  }
}

//router.post('/snack', addSnack)
function addSnack(req, res) {
  if (req.body && req.body.snack) {

    const newSnack = new Snack(req.body.snack)
    newSnack.save(err => {
      if (err) {
        send404err(err, res)
      }
      res.json({success: true, message: 'Allah Snack Bar !!!'})
      res.end()
    })

  } else {
    send400InvalidParams(res)
  }
}

// router.get('/snack/promo', getPromo)
function getPromo(req, res) {
  Snack.find({promo:true}, (err, snacks) => {
    if (err) {
      send404err(err, res)
    }
    res.json({success: true, snacks:snacks})
    res.end()
  })
}

// router.get('/snack/hard', getHard)
function getHard(req, res) {
  Snack.find({hard:true}, (err, snacks) => {
    if (err) {
      send404err(err, res)
    }
    res.json({success: true, snacks:snacks})
    res.end()
  })
}

// router.get('/snack/soft', getSoft)
function getSoft(req, res) {
  Snack.find({hard:false}, (err, snacks) => {
    if (err) {
      send404err(err, res)
    }
    res.json({success: true, snacks:snacks})
    res.end()
  })
}

/*
//
//  ORDER ROUTE
//
*/


// router.post('/order', makeOrder)
function makeOrder(req, res) {
  if (req.body && req.body.mail && req.body.order) {

    const mail = req.body.mail
    const order = req.body.order

    if (!mail || !order) {
      res.status(404)
      res.json({success: false, message: "mail || order is blank"})
      res.end()
    }

    User.findOne({ mail }, (err, user) => {
      if (err) {
        send404err(err, res)
      } else if (!user) {
        sen401UserNotFound(res)
      } else if (user) {
        const orderPrice = order.totalAmount
        const userCreditAmount = user.creditAmount

        if (orderPrice > userCreditAmount) {
          res.json({
            success: true,
            message: 'Error: not enought money in the bank. Please refill your account.'
          })
          res.end()
        }

        const oldOrderList = user.orders
        let newOrderList
        if (!oldOrderList) {
          newOrderList = order
        } else {
          newOrderList = oldOrderList
          console.log("1 =>", newOrderList);
          console.log("2 =>", order);
          newOrderList.push(order)
          console.log("3 =>", newOrderList);
        }

        const newCreditAmount = userCreditAmount - orderPrice
        user.update({$set: {creditAmount:newCreditAmount, orders:newOrderList}}, (err, message) => {
          console.log(message);
          if (err) {
            res.status(400);
        		res.json({yolo: true, success: false, err})
            res.end()
          }
          res.json({success: true, message: 'Order ordered lol !'})
          res.end()
        })
      }
    })

  } else {
    send400InvalidParams(res)
  }
}

// router.get('/profile/:id/history', getOrderHistory)
function getOrderHistory(req, res) {
  if (req.params && req.params.id) {

    const userId = req.params.id

    if (!userId) {
      res.status(404)
      res.json({success: false, message: "userId is blank"})
      res.end()
    }

    User.findById(userId, 'creditAmount orders', (err, user) =>{
      if (err) {
        send404err(err, res)
      } else if (!user) {
        sen401UserNotFound(res)
      } else if (user) {
        res.json({success: true, user})
        res.end()
      }
    })

  } else {
    send400InvalidParams(res)
  }
}

module.exports = router
